EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Raspberry_Pi_2_3 J1
U 1 1 5F81F9A2
P 2000 3800
F 0 "J1" H 2000 5371 50  0000 C CNN
F 1 "Raspberry_Pi_2_3" H 2000 5280 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x20_P2.54mm_Vertical" H 2000 5189 50  0000 C CNN
F 3 "https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/rpi_SCH_3bplus_1p0_reduced.pdf" H 2000 3800 50  0001 C CNN
	1    2000 3800
	1    0    0    -1  
$EndComp
$Comp
L Regulator_SwitchedCapacitor:ICL7660 U2
U 1 1 5F8240AD
P 8000 2750
F 0 "U2" H 8000 3317 50  0000 C CNN
F 1 "ICL7660" H 8000 3226 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 8100 2650 50  0001 C CNN
F 3 "http://datasheets.maximintegrated.com/en/ds/ICL7660-MAX1044.pdf" H 8100 2650 50  0001 C CNN
	1    8000 2750
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM741 U3
U 1 1 5F824F5B
P 7850 4050
F 0 "U3" H 8194 4096 50  0000 L CNN
F 1 "LM741" H 8194 4005 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 7900 4100 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 8000 4200 50  0001 C CNN
	1    7850 4050
	1    0    0    -1  
$EndComp
$Comp
L LightcontrolV2-rescue:Interface_Expansion_PCF8591-LightcontrolV2-cache U1
U 1 1 5F835C2E
P 4850 3500
F 0 "U1" H 4850 4571 50  0000 C CNN
F 1 "Interface_Expansion_PCF8591" H 4850 4480 50  0000 C CNN
F 2 "LightcontrolV2:PCF8591_Expansion_Board" H 4850 4389 50  0000 C CNN
F 3 "" H 4850 3300 50  0001 C CNN
	1    4850 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 3200 4250 3200
Wire Wire Line
	2800 3300 4250 3300
$Comp
L power:+5V #PWR0101
U 1 1 5F839548
P 4850 1600
F 0 "#PWR0101" H 4850 1450 50  0001 C CNN
F 1 "+5V" H 4865 1773 50  0000 C CNN
F 2 "" H 4850 1600 50  0001 C CNN
F 3 "" H 4850 1600 50  0001 C CNN
	1    4850 1600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5F839F86
P 4850 4300
F 0 "#PWR0102" H 4850 4050 50  0001 C CNN
F 1 "GND" H 4855 4127 50  0000 C CNN
F 2 "" H 4850 4300 50  0001 C CNN
F 3 "" H 4850 4300 50  0001 C CNN
	1    4850 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 2700 4850 2450
Wire Wire Line
	4850 4000 4850 4150
Wire Wire Line
	5450 3600 6400 3600
Wire Wire Line
	6400 3600 6400 3950
Wire Wire Line
	6400 3950 7550 3950
$Comp
L Converter_DCDC:MEE3S0512SC PS1
U 1 1 5F84222A
P 6600 2550
F 0 "PS1" H 6600 2917 50  0000 C CNN
F 1 "MEE3S0512SC" H 6600 2826 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_Murata_MEE3SxxxxSC_THT" H 5550 2300 50  0001 L CNN
F 3 "https://power.murata.com/pub/data/power/ncl/kdc_mee3.pdf" H 7650 2250 50  0001 L CNN
	1    6600 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2450 4850 2450
Connection ~ 4850 2450
Wire Wire Line
	6200 2650 6200 3000
Wire Wire Line
	6200 4150 4850 4150
Connection ~ 4850 4150
Wire Wire Line
	4850 4150 4850 4300
Wire Wire Line
	7000 2650 7000 3000
Wire Wire Line
	7000 3000 6200 3000
Connection ~ 6200 3000
Wire Wire Line
	6200 3000 6200 3400
Wire Wire Line
	7000 2450 7300 2450
Wire Wire Line
	8000 3250 8000 3400
Wire Wire Line
	8000 3400 6200 3400
Connection ~ 6200 3400
Wire Wire Line
	6200 3400 6200 4150
Wire Wire Line
	7300 2450 7300 3650
Wire Wire Line
	7300 3650 7750 3650
Wire Wire Line
	7750 3650 7750 3750
Connection ~ 7300 2450
Wire Wire Line
	7300 2450 7600 2450
Wire Wire Line
	8400 2450 8800 2450
Wire Wire Line
	8800 2450 8800 3400
Wire Wire Line
	8800 4800 8100 4800
Wire Wire Line
	7750 4800 7750 4350
$Comp
L Device:CP C2
U 1 1 5F847F51
P 8600 2850
F 0 "C2" H 8718 2896 50  0000 L CNN
F 1 "10 µF" H 8718 2805 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 8638 2700 50  0001 C CNN
F 3 "~" H 8600 2850 50  0001 C CNN
	1    8600 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 2650 8600 2650
Wire Wire Line
	8600 2650 8600 2700
Wire Wire Line
	8600 3000 8600 3050
Wire Wire Line
	8600 3050 8400 3050
$Comp
L Device:CP C3
U 1 1 5F849ABB
P 8450 3400
F 0 "C3" V 8705 3400 50  0000 C CNN
F 1 "10 µF" V 8614 3400 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 8488 3250 50  0001 C CNN
F 3 "~" H 8450 3400 50  0001 C CNN
	1    8450 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8300 3400 8000 3400
Connection ~ 8000 3400
Wire Wire Line
	8600 3400 8800 3400
Connection ~ 8800 3400
Wire Wire Line
	8800 3400 8800 4800
$Comp
L Device:R_POT_Small R9
U 1 1 5F84B4C6
P 8100 4600
F 0 "R9" V 7903 4600 50  0000 C CNN
F 1 "100k" V 7994 4600 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_ACP_CA6-H2,5_Horizontal" H 8100 4600 50  0001 C CNN
F 3 "~" H 8100 4600 50  0001 C CNN
	1    8100 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	8000 4600 7850 4600
Wire Wire Line
	7850 4600 7850 4350
Wire Wire Line
	7950 4350 8450 4350
Wire Wire Line
	8450 4350 8450 4600
Wire Wire Line
	8450 4600 8200 4600
Wire Wire Line
	8100 4700 8100 4800
Connection ~ 8100 4800
Wire Wire Line
	8100 4800 7750 4800
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 5F84E054
P 10250 4050
F 0 "J2" H 10278 4026 50  0000 L CNN
F 1 "Conn_01x02_Female" H 10278 3935 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 10250 4050 50  0001 C CNN
F 3 "~" H 10250 4050 50  0001 C CNN
	1    10250 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 4050 9100 4050
$Comp
L Device:R_POT_Small R10
U 1 1 5F8508D9
P 9100 5000
F 0 "R10" H 9040 4954 50  0000 R CNN
F 1 "5M" H 9040 5045 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_ACP_CA6-H2,5_Horizontal" H 9100 5000 50  0001 C CNN
F 3 "~" H 9100 5000 50  0001 C CNN
	1    9100 5000
	-1   0    0    1   
$EndComp
$Comp
L Device:R R11
U 1 1 5F851AE3
P 9100 5550
F 0 "R11" H 9170 5596 50  0000 L CNN
F 1 "1M" H 9170 5505 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 9030 5550 50  0001 C CNN
F 3 "~" H 9100 5550 50  0001 C CNN
	1    9100 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 4050 9100 4900
Connection ~ 9100 4050
Wire Wire Line
	9100 5100 9100 5250
Wire Wire Line
	9100 5250 8900 5250
Wire Wire Line
	8900 5250 8900 5000
Wire Wire Line
	8900 5000 9000 5000
Connection ~ 9100 5250
Wire Wire Line
	9100 5250 9100 5400
Wire Wire Line
	8900 5250 7350 5250
Wire Wire Line
	7350 5250 7350 4150
Wire Wire Line
	7350 4150 7550 4150
Connection ~ 8900 5250
Wire Wire Line
	9100 5700 9100 6000
Wire Wire Line
	9100 6000 6200 6000
Wire Wire Line
	6200 6000 6200 4150
Connection ~ 6200 4150
Connection ~ 9100 6000
Wire Wire Line
	10050 6000 10050 4150
Wire Wire Line
	9100 6000 10050 6000
Wire Wire Line
	9100 4050 10050 4050
Wire Wire Line
	4850 4150 3600 4150
Wire Wire Line
	3600 4150 3600 5400
Wire Wire Line
	3600 5400 2300 5400
Wire Wire Line
	2300 5400 2300 5100
Wire Wire Line
	6000 1600 4850 1600
Wire Wire Line
	4850 1600 4850 2450
Connection ~ 4850 1600
Wire Wire Line
	6000 1700 5850 1700
Wire Wire Line
	5850 1700 5850 3000
Wire Wire Line
	5850 3000 6200 3000
Wire Wire Line
	1600 5100 1700 5100
Connection ~ 2300 5100
Connection ~ 1700 5100
Wire Wire Line
	1700 5100 1800 5100
Connection ~ 1800 5100
Wire Wire Line
	1800 5100 1900 5100
Connection ~ 1900 5100
Wire Wire Line
	1900 5100 2000 5100
Connection ~ 2000 5100
Wire Wire Line
	2000 5100 2100 5100
Connection ~ 2100 5100
Wire Wire Line
	2100 5100 2200 5100
Connection ~ 2200 5100
Wire Wire Line
	2200 5100 2300 5100
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 5F88C9BA
P 6200 1600
F 0 "J3" H 6228 1576 50  0000 L CNN
F 1 "Conn_01x02_Female" H 6228 1485 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 6200 1600 50  0001 C CNN
F 3 "~" H 6200 1600 50  0001 C CNN
	1    6200 1600
	1    0    0    -1  
$EndComp
$EndSCHEMATC
